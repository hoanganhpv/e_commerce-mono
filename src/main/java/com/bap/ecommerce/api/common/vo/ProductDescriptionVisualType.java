package com.bap.ecommerce.api.common.vo;

public enum ProductDescriptionVisualType {
    IMAGE, VIDEO;
}
