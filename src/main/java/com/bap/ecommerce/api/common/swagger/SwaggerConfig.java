package com.bap.ecommerce.api.common.swagger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.responses.ApiResponse;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.bap.ecommerce.api.common.swagger.SwaggerUtils.methodToReadInFileToString;

@Configuration
public class SwaggerConfig {

    private final String jsonDir = "src/main/resources/static/swagger/json/";

    @Bean
    public OpenApiCustomizer applyStandardOpenAPIModifications() {
        return openApi -> {
            Paths paths = new Paths();
            openApi.getPaths().entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .forEach(stringPathItemEntry -> {
                        paths.addPathItem(stringPathItemEntry.getKey(), addExamples(stringPathItemEntry.getValue()));
                    });
            openApi.setPaths(paths);
        };
    }

    private PathItem addExamples(PathItem pathItem) {
         Map<PathItem.HttpMethod, Operation> operations = new LinkedHashMap<>();

        operations.put(PathItem.HttpMethod.GET, pathItem.getGet());
        operations.put(PathItem.HttpMethod.PUT, pathItem.getPut());
        operations.put(PathItem.HttpMethod.POST, pathItem.getPost());
        operations.put(PathItem.HttpMethod.DELETE, pathItem.getDelete());
        operations.put(PathItem.HttpMethod.PATCH, pathItem.getPatch());

        operations.forEach((httpMethod, operation) -> {

            if(operation != null) {
                Map<String, ApiResponse> responseMap = operation.getResponses();

                responseMap.entrySet().forEach(entry -> {

                    ApiResponse response = entry.getValue();
                    Content content = response.getContent();

                    content.values().forEach(mediaType -> {
                        Object example = mediaType.getExample();

                        if(example != null && example.toString().startsWith("@")) {
                            String fileName = example.toString().replaceFirst("@", "");
                            String filePath = jsonDir + fileName;

                            try {
                                JsonNode node = new ObjectMapper().readTree(methodToReadInFileToString(filePath));

                                mediaType.setExample(node);

                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        }
                    });
                });

                RequestBody requestBody = operation.getRequestBody();
                if(requestBody != null) {
                    Content requestBodyContent = requestBody.getContent();

                    requestBodyContent.values().forEach(mediaType -> {
                        Object example = mediaType.getExample();

                        if (example != null && example.toString().startsWith("@")) {
                            String fileName = example.toString().replaceFirst("@", "");
                            String filePath = jsonDir + fileName;

                            try {
                                JsonNode node = new ObjectMapper().readTree(methodToReadInFileToString(filePath));
                                mediaType.setExample(node);
                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        }
                    });
                }

                String descriptionFileName = operation.getDescription();

                if (descriptionFileName != null && descriptionFileName.startsWith("@")) {
                    String fileName = descriptionFileName.replaceFirst("@", "");
                    String filePath = jsonDir + fileName;

                    try {
                        String description = methodToReadInFileToString(filePath);
                        operation.setDescription(description);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }

            }
        });

        return pathItem;
    }
}
