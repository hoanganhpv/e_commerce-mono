package com.bap.ecommerce.api.common.entity;

import java.time.Instant;
import java.util.LinkedHashSet;
import java.util.Set;

import com.bap.ecommerce.api.base.entity.BaseAuditEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "user_payment_method")
public class UserPaymentMethod extends BaseAuditEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "method")
    private Short method;

    @Column(name = "is_default")
    private Boolean isDefault;

    @Column(name = "card_token")
    private String cardToken;

    @Column(name = "card_exp")
    private Instant cardExp;

    @OneToMany(mappedBy = "paymentMethod")
    private Set<Order> orders = new LinkedHashSet<>();
}
