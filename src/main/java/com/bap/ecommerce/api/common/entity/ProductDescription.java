package com.bap.ecommerce.api.common.entity;

import java.util.LinkedHashSet;
import java.util.Set;

import com.bap.ecommerce.api.base.entity.BaseAuditEntity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "product_description")
public class ProductDescription extends BaseAuditEntity {

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
    private Product product;

    @Column(name = "descriptions", length = Integer.MAX_VALUE)
    private String descriptions;

    @OneToMany(mappedBy = "productDescription", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ProductDescriptionVisual> productDescriptionVisuals = new LinkedHashSet<>();

    public ProductDescription(Product product, String descriptions) {
        this.product = product;
        this.descriptions = descriptions;
    }
}
