package com.bap.ecommerce.api.common.vo;

import lombok.*;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum UserStatusEnum {
    NOT_ACTIVATED(0),
    ACTIVATED(1),
    BANNED(2);

    private int value;
}
