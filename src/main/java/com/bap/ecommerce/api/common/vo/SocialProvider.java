package com.bap.ecommerce.api.common.vo;

public enum SocialProvider {
    Google, Facebook
}
