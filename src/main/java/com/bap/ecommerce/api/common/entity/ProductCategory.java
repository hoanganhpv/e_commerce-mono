package com.bap.ecommerce.api.common.entity;

import java.util.LinkedHashSet;
import java.util.Set;

import com.bap.ecommerce.api.base.entity.BaseAuditEntity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "product_categories")
public class ProductCategory extends BaseAuditEntity {

    @Column(name = "title", length = 100)
    private String title;

    @Column(name = "slug")
    private String slug;

    @Column(name = "descriptions")
    private String descriptions;

    @Column(name = "parent_id")
    private Short parentId;

    @Column(name = "status")
    private Short status;

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Product> products = new LinkedHashSet<>();
}
