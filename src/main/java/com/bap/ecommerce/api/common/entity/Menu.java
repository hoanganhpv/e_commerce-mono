package com.bap.ecommerce.api.common.entity;

import com.bap.ecommerce.api.base.entity.BaseAuditEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "menu")
public class Menu extends BaseAuditEntity {

    @Column(name = "parent_id")
    private Short parentId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "privilege_id")
    private Privilege privilege;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "slug", length = 100)
    private String slug;

    @Column(name = "icon", length = 100)
    private String icon;

    @Column(name = "description")
    private String description;
}
