package com.bap.ecommerce.api.common.vo;

public enum ProductRatingVisualType {
    IMAGE, VIDEO;
}
