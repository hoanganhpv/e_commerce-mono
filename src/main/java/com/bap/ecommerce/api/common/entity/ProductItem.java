package com.bap.ecommerce.api.common.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.bap.ecommerce.api.base.entity.BaseAuditEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "product_items")
public class ProductItem extends BaseAuditEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;

    @Column(name = "title")
    private String title;

    @Column(name = "price", columnDefinition = "numeric(10,2)")
    private BigDecimal price;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "status")
    private Boolean status;

    @OneToMany(mappedBy = "productItem")
    private List<ProductItemDetail> productItemDetails = new ArrayList<>();
    @OneToMany(mappedBy = "productItem")
    private List<OrderDetail> orderDetail = new ArrayList<>();
}
