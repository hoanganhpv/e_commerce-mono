package com.bap.ecommerce.api.common.exception;


public class AuthorizeException extends RuntimeException{

    public AuthorizeException(String message) {
        super(message);
    }
}
