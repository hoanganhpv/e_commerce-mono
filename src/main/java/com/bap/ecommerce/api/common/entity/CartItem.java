package com.bap.ecommerce.api.common.entity;

import com.bap.ecommerce.api.base.entity.BaseAuditEntity;
import com.bap.ecommerce.api.common.vo.CartItemStatusEnum;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "cart_items")
public class CartItem extends BaseAuditEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_item_id")
    ProductItem productItem;

    @Column(name = "quantity")
    private Short quantity;

    @Column(name = "status")
    private CartItemStatusEnum status;
}
