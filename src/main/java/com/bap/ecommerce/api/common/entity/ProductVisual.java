package com.bap.ecommerce.api.common.entity;

import com.bap.ecommerce.api.base.entity.BaseAuditEntity;
import com.bap.ecommerce.api.common.vo.ProductVisualType;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "product_visual")
public class ProductVisual extends BaseAuditEntity {

    @Column(name = "type")
    private ProductVisualType type;

    @Column(name = "url")
    private String url;

    @Column(name = "slug")
    private String slug;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_option_detail_id")
    private ProductOptionDetail productOptionDetail;

}
