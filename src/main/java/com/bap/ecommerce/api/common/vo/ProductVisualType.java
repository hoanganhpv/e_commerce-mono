package com.bap.ecommerce.api.common.vo;

public enum ProductVisualType {
    IMAGE,
    VIDEO;
}
