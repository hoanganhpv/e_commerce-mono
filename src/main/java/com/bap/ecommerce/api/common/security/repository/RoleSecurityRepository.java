package com.bap.ecommerce.api.common.security.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bap.ecommerce.api.common.entity.Role;
import com.bap.ecommerce.api.common.security.model.ERole;

@Repository
public interface RoleSecurityRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(ERole name);
}
