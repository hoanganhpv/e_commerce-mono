package com.bap.ecommerce.api.common.vo;

public enum OrderStatusE {
    DELIVERED, SHIPPED, CANCELED, PENDING, CLOSED,PAID;
}
