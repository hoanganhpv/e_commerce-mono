package com.bap.ecommerce.api.common.entity;

import java.util.ArrayList;
import java.util.List;

import com.bap.ecommerce.api.base.entity.BaseAuditEntity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "product_option_detail")
public class ProductOptionDetail extends BaseAuditEntity {

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_option_id")
    private ProductOption productOption;

    @Column(name = "value", length = 50)
    private String value;

    @OneToMany(mappedBy = "productOptionDetail")
    private List<ProductVisual> productVisuals = new ArrayList<>();
}
