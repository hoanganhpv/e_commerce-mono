package com.bap.ecommerce.api.common.security.model;

public enum ERole {
    ROLE_ADMIN,
    ROLE_STAFF,
    ROLE_SELLER,
    ROLE_USER,
    ROLE_GUEST
}
