package com.bap.ecommerce.api.common.entity;

import java.util.ArrayList;
import java.util.List;

import com.bap.ecommerce.api.base.entity.BaseAuditEntity;
import com.bap.ecommerce.api.common.vo.OrderStatusE;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "orders")
public class Order extends BaseAuditEntity {

    @Column(name = "status")
    private OrderStatusE status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "payment_method")
    private UserPaymentMethod paymentMethod;

    @Column(name = "delivery_method")
    private Short deliveryMethod;

    @OneToMany(mappedBy = "order")
    private List<OrderDetail> orderDetails = new ArrayList<>();
}
