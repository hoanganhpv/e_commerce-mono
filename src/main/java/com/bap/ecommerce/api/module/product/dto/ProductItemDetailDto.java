package com.bap.ecommerce.api.module.product.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductItemDetailDto {

    private Long id;

    private Long productItemId;

    private Long productOptionDetailId;
}
