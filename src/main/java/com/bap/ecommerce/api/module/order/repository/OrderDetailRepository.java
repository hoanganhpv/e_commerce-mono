package com.bap.ecommerce.api.module.order.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bap.ecommerce.api.common.entity.OrderDetail;
import com.bap.ecommerce.api.module.order.dto.OrderDetailDto;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long> {

    @Query("SELECT new com.bap.ecommerce.api.module.order.dto.OrderDetailDto(odd.id,odd.order.id,odd.productItem.id,odd.quantity,odd.status)FROM OrderDetail odd  WHERE odd.order.id = :id")
    List<OrderDetailDto> getOrderDetailByOrderId(Long id);
}
