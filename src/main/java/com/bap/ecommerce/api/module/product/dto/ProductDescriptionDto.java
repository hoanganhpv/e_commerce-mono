package com.bap.ecommerce.api.module.product.dto;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ProductDescriptionDto {

    private Long productId;

    private String description;

    private List<ProductDescriptionVisualDto> listMedia;
}
