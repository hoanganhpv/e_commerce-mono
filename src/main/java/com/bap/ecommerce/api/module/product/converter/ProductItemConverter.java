package com.bap.ecommerce.api.module.product.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bap.ecommerce.api.common.entity.ProductItem;
import com.bap.ecommerce.api.module.product.dto.ProductItemDetailDto;
import com.bap.ecommerce.api.module.product.dto.ProductItemDto;

@Component
public class ProductItemConverter {

    @Autowired
    private ProductItemDetailConverter productItemDetailConverter;

    public ProductItemDto toDto(ProductItem entity) {
        ProductItemDto dto = new ProductItemDto();
        dto.setId(entity.getId());
        dto.setProductId(entity.getProduct().getId());
        dto.setPrice(entity.getPrice());
        dto.setQuantity(entity.getQuantity());
        List<ProductItemDetailDto> productItemDetails = entity.getProductItemDetails()
                .stream()
                .map(e -> productItemDetailConverter.toDto(e))
                .collect(Collectors.toList());
        dto.setProductItemDetails(productItemDetails);
        return dto;
    }
}
