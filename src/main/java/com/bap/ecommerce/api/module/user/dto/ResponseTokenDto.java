package com.bap.ecommerce.api.module.user.dto;

import com.bap.ecommerce.api.common.dto.ResponseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseTokenDto extends ResponseDto {
    private String token;
}
