package com.bap.ecommerce.api.module.product.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bap.ecommerce.api.common.entity.ProductOption;
import com.bap.ecommerce.api.module.product.dto.ProductOptionDetailDto;
import com.bap.ecommerce.api.module.product.dto.ProductOptionDto;

@Component
public class ProductOptionConverter {

    @Autowired
    private ProductOptionDetailConverter productOptionDetailConverter;

    /**
     * Convert ProductOption to ProductOptionDto
     *
     * @param entity ProductOption
     * @return ProductOptionDto with List ProductOptionDetailDto
     */
    public ProductOptionDto toDto(ProductOption entity) {
        ProductOptionDto dto = new ProductOptionDto();
        dto.setId(entity.getId());
        dto.setProductId(entity.getProduct().getId());
        dto.setOption(entity.getOption());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setUpdatedAt(entity.getUpdatedAt());
        List<ProductOptionDetailDto> listProductOptionDetail = entity.getProductOptionDetails()
                .stream()
                .map(e -> productOptionDetailConverter.toDto(e))
                .collect(Collectors.toList());
        dto.setListProductOptionDetail(listProductOptionDetail);
        return dto;
    }
}
