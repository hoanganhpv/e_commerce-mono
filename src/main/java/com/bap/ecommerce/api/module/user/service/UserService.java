package com.bap.ecommerce.api.module.user.service;

import com.bap.ecommerce.api.common.vo.SocialProvider;
import com.bap.ecommerce.api.module.user.dto.*;
import jakarta.servlet.http.HttpServletRequest;

import java.util.HashMap;

public interface UserService {

    void saveUser(RegisterDto registerDto);

    HashMap<String, String> loginByEmailAndPassword(LoginDto loginDto);

    void sendVerificationCode(EmailDto emailDto);

    void confirmVerificationCodeAndActiveUser(VerificationCodeDto verificationCodeDto);

    void socialAccountHandler(String email, SocialProvider provider);

    void updatePassword(ChangePasswordDto changePasswordDto);

    String confirmChangePasswordByVerificationCode(VerificationCodeDto verificationCodeDto);

    void deleteUserById(Long id);

    void saveUserBySocial(SocialLoginDto socialLoginDto);

    HashMap<String, Object> refreshToken(String refresh_token);

    Long getUserIdFromToken(HttpServletRequest request);
}
