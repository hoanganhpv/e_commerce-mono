package com.bap.ecommerce.api.module.cart.operation;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Operation(
        description = "@create_new_cart_item_descriptions.json",
        requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                content = @Content(mediaType = "application/json",
                        examples = {
                                @ExampleObject(
                                        value = "@create_new_cart_item_request_body.json"
                                )
                        }
                )),
        responses = {
                @ApiResponse(responseCode = "200", description = "Create new cart item successfully",
                        content = @Content(mediaType = "application/json",
                                examples = {
                                        @ExampleObject(
                                                value = "@create_new_cart_item_200.json"
                                        )
                                }
                        )
                ),
                @ApiResponse(responseCode = "400", description = "Bad request!",
                        content = @Content(mediaType = "application/json",
                                examples = {
                                        @ExampleObject(
                                                value = "@create_new_cart_item_400.json"
                                        )
                                }
                        )
                ),
                @ApiResponse(responseCode = "403", description = "Unauthorized",
                        content = @Content(mediaType = "application/json",
                                examples = {
                                        @ExampleObject(
                                                value = "@create_new_cart_item_403.json"
                                        )
                                }
                        )
                ),
                @ApiResponse(responseCode = "404", description = "Resource not found!",
                        content = @Content(mediaType = "application/json",
                                examples = {
                                        @ExampleObject(
                                                value = "@create_new_cart_item_404.json"
                                        )
                                }
                        )
                )
        }
)
public @interface CreateNewCartItemOperation {
}
