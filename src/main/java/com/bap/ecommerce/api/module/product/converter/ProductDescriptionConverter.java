package com.bap.ecommerce.api.module.product.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bap.ecommerce.api.common.entity.ProductDescription;
import com.bap.ecommerce.api.module.product.dto.ProductDescriptionDto;
import com.bap.ecommerce.api.module.product.dto.ProductDescriptionVisualDto;

@Component
public class ProductDescriptionConverter {

    @Autowired
    private ProductDescriptionVisualConverter productDescriptionVisualConverter;

    /**
     * Convert ProductDescription to ProductDescriptionDto
     *
     * @param entity ProductDescription
     * @return ProductDescriptionDto with List ProductDescriptionVisualDto
     */
    public ProductDescriptionDto toDto(ProductDescription entity) {
        ProductDescriptionDto dto = new ProductDescriptionDto();
        dto.setProductId(entity.getProduct().getId());
        dto.setDescription(entity.getDescriptions());
        List<ProductDescriptionVisualDto> listMedia = entity.getProductDescriptionVisuals()
                .stream()
                .map(e -> productDescriptionVisualConverter.toDto(e))
                .collect(Collectors.toList());
        dto.setListMedia(listMedia);
        return dto;
    }
}
