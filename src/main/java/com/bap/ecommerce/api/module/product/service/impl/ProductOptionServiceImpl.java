package com.bap.ecommerce.api.module.product.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bap.ecommerce.api.common.entity.ProductOption;
import com.bap.ecommerce.api.common.vo.ProductStatusType;
import com.bap.ecommerce.api.module.product.converter.ProductOptionConverter;
import com.bap.ecommerce.api.module.product.dto.ProductOptionDto;
import com.bap.ecommerce.api.module.product.repository.ProductOptionRepository;
import com.bap.ecommerce.api.module.product.service.ProductOptionService;

@Service
public class ProductOptionServiceImpl implements ProductOptionService {

    @Autowired
    private ProductOptionRepository productOptionRepository;

    @Autowired
    private ProductOptionConverter productOptionConverter;

    /**
     * *
     * Find Product Options ByProduct id
     *
     * @param productId This is id of product.
     * @return List of Product Options with a List of Image and Video in each
     *         Product Option Detail on each Option
     */
    public List<ProductOptionDto> findProductOptionByProductId(Long productId) {
        List<ProductStatusType> status = List.of(ProductStatusType.RECYCLE,
                ProductStatusType.DRAFT,
                ProductStatusType.DELETED);
        List<ProductOption> pageListEntity = this.productOptionRepository.findProductOptionByProductId(productId,
                status);

        if (pageListEntity.isEmpty()) {
            return null;
        }

        return pageListEntity.stream()
                .map(item -> this.productOptionConverter.toDto(item))
                .collect(Collectors.toList());
    }
}
