package com.bap.ecommerce.api.module.product.converter;

import org.springframework.stereotype.Component;

import com.bap.ecommerce.api.common.entity.ProductItemDetail;
import com.bap.ecommerce.api.module.product.dto.ProductItemDetailDto;

@Component
public class ProductItemDetailConverter {

    public ProductItemDetailDto toDto(ProductItemDetail entity) {
        ProductItemDetailDto dto = new ProductItemDetailDto();
        dto.setId(entity.getId());
        dto.setProductItemId(entity.getProductItem().getId());
        dto.setProductOptionDetailId(entity.getProductOptionDetail().getId());
        return dto;
    }
}
