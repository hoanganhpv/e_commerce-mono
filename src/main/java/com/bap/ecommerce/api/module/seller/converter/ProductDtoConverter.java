package com.bap.ecommerce.api.module.seller.converter;



import com.bap.ecommerce.api.common.entity.Product;
import com.bap.ecommerce.api.common.entity.ProductVisual;
import com.bap.ecommerce.api.module.seller.dto.ProductDto;
import com.bap.ecommerce.api.module.seller.dto.ProductListDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ProductDtoConverter {


    /**
     * *
     * This method to map ProductEntity to ProductDto
     *
     * @param product This is  entity to map to ProductDto.
     * @return productDto.
     */
    public ProductListDto mapToProduct(Product product) {
        ProductListDto productListDto = ProductListDto.builder()
                .id(product.getId())
                .title(product.getTitle())
                .quantityInStock(product.getQuantityInStock())
                .price(product.getPrice())
                .status(product.getStatus())
                .categoryTitle(product.getCategory() == null ? null : product.getCategory().getTitle())
                .url(product.getProductVisuals().stream()
                        .map(ProductVisual::getUrl) // Giả sử trường URL có tên là "url", thay thế nếu cần.
                        .findFirst() // Lấy đối tượng đầu tiên trong Stream (hoặc Optional nếu không tìm thấy).
                        .orElse(null))
                .build();
        return productListDto;
    }
}

