package com.bap.ecommerce.api.module.product.dto;

import com.bap.ecommerce.api.common.vo.ProductDescriptionVisualType;

import java.time.Instant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDescriptionVisualDto {
    private Long id;

    private ProductDescriptionVisualType type;

    private String url;

    private Instant createdAt;

    private Instant updatedAt;

}
