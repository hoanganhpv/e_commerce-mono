package com.bap.ecommerce.api.module.seller.dto;

import com.bap.ecommerce.api.common.vo.ProductStatusType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Builder

public class ProductListDto {
    private Long id;

    private String title;

    private Integer quantityInStock;

    private BigDecimal price;

    private ProductStatusType status;

    private String categoryTitle;

    private String url;
}