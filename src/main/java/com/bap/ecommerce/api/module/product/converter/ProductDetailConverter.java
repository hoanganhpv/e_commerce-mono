package com.bap.ecommerce.api.module.product.converter;

import org.springframework.stereotype.Component;

import com.bap.ecommerce.api.common.entity.ProductDetail;
import com.bap.ecommerce.api.module.product.dto.ProductDetailDto;

@Component
public class ProductDetailConverter {

    public ProductDetailDto toDto(ProductDetail entity) {
        ProductDetailDto dto = new ProductDetailDto();
        dto.setId(entity.getId());
        dto.setProductId(entity.getProduct().getId());
        dto.setKey(entity.getKey());
        dto.setValue(entity.getValue());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setUpdatedAt(entity.getUpdatedAt());
        return dto;
    }
}
