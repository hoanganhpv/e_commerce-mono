package com.bap.ecommerce.api.module.seller.service;

import java.util.HashMap;
import java.util.List;

import com.bap.ecommerce.api.module.seller.dto.*;
import org.springframework.web.multipart.MultipartFile;

public interface SellerService {

    HashMap<String, String> login(LoginDto loginDto);

    Long saveProduct(ProductDto productDto);

    void updateProduct(ProductDto productDto);

    void saveProductDetail(ProductDetailsDto productDetailsDto);

    void saveProductDescription(ProductDescriptionDto productDescriptionDto);

    void saveProductOptionWithItem(ProductOptionWithItemDto productOptionWithItemDto);

    List<ProductDescriptionVisualDto> uploadProductFile(MultipartFile[] file);

    ProductOptionWithItemDto getProductOptionWithItem(Long id);

    ProductDescriptionDto getProductDescription(Long id);

    ProductDto getProduct(Long id);

    ProductDetailsDto getProductDetails(Long id);

    List<ProductCategoryDto> getAllProductCategory();

    PageProductResDto getAllProductForSeller(Long sellerId,int perPage, int currentPage);
}
