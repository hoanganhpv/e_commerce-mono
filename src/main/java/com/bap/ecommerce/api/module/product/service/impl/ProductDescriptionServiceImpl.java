package com.bap.ecommerce.api.module.product.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bap.ecommerce.api.common.entity.ProductDescription;
import com.bap.ecommerce.api.common.vo.ProductStatusType;
import com.bap.ecommerce.api.module.product.converter.ProductDescriptionConverter;
import com.bap.ecommerce.api.module.product.dto.ProductDescriptionDto;
import com.bap.ecommerce.api.module.product.repository.ProductDescriptionRepository;
import com.bap.ecommerce.api.module.product.service.ProductDescriptionService;

@Service
public class ProductDescriptionServiceImpl implements ProductDescriptionService {

    @Autowired
    private ProductDescriptionRepository productDescriptionRepository;

    @Autowired
    private ProductDescriptionConverter productDescriptionConverter;

    /**
     * *
     * Find Product Description ByProduct ID
     *
     * @param productId This is id of product.
     * @return List of Product Description with a List of Image and Video (Visual
     *         Description) on each Description
     */
    public List<ProductDescriptionDto> findProductDescriptionByProductId(Long productId) {
        List<ProductStatusType> status = List.of(ProductStatusType.RECYCLE,
                ProductStatusType.DRAFT,
                ProductStatusType.DELETED);
        List<ProductDescription> pageListEntity = this.productDescriptionRepository.findByProduct(productId, status);
        if (pageListEntity.isEmpty()) {
            return null;
        }

        return pageListEntity.stream()
                .map(item -> this.productDescriptionConverter.toDto(item))
                .collect(Collectors.toList());
    }
}
