package com.bap.ecommerce.api.module.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bap.ecommerce.api.common.entity.ProductRatingVisual;

@Repository
public interface ProductRatingVisualRepository extends JpaRepository<ProductRatingVisual, Long> {

}
