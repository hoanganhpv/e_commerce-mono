package com.bap.ecommerce.api.module.product.dto;

import com.bap.ecommerce.api.common.dto.ResponseDto;

@lombok.Getter
@lombok.Setter

public class ResponseDataDto extends ResponseDto {
    private Object data;
}
