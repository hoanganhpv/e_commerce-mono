package com.bap.ecommerce.api.module.cart.operation;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Operation(
        description = "Perform deletion based on the cart item flag of the user when " +
                "the user clicks the remove button to remove a product from the shopping cart.",
        parameters = {
                @Parameter(name = "userId", description = "The ID of the user who is removing" +
                        " the product from the shopping cart", example = "2", required = true),
                @Parameter(name = "cartItemId", description = "The ID of the product that the " +
                        "user is removing from the shopping cart", example = "10", required = true)
        },
        responses = {
                @ApiResponse(responseCode = "200", description = "Get all user's cart item successfully!",
                        content = @Content(mediaType = "application/json",
                                examples = {
                                        @ExampleObject(
                                                value = "@delete_cart_item_200.json")
                                }
                        )
                ),
                @ApiResponse(responseCode = "403", description = "Unauthorized",
                        content = @Content(mediaType = "application/json",
                                examples = {
                                        @ExampleObject(
                                                value = "@delete_cart_item_403.json"
                                        )
                                }
                        )
                ),
                @ApiResponse(responseCode = "404", description = "Resource not found!",
                        content = @Content(mediaType = "application/json",
                                examples = {
                                        @ExampleObject(
                                                value = "@delete_cart_item_404.json"
                                        )
                                }
                        )
                )
        }
)
public @interface DeleteCartItemOperation {
}
