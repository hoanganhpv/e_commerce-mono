package com.bap.ecommerce.api.module.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bap.ecommerce.api.common.entity.SocialAccount;
import com.bap.ecommerce.api.common.vo.SocialProvider;

@Repository
public interface UserSocialRepository extends JpaRepository<SocialAccount, Integer> {

    @Query("SELECT u FROM SocialAccount u WHERE u.socialAccountUserId = :socialAccountUserId")
    Optional<SocialAccount> getSocialAccountUserId(String socialAccountUserId);

    @Query("SELECT u FROM SocialAccount u " +
            "WHERE u.socialAccountUserId = :socialAccountUserId " +
            "and u.provider = :provider")
    Optional<SocialAccount> getSocialAccountUserIdAndProvider(String socialAccountUserId,
            SocialProvider provider);
}
