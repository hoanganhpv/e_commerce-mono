package com.bap.ecommerce.api.module.seller.dto;


import com.bap.ecommerce.api.common.vo.ProductVisualType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductOptionDetailImageDto {

    private String url;
    private ProductVisualType type;

}
