package com.bap.ecommerce.api.module.product.dto;

import java.time.Instant;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ProductDetailDto {
    private Long id;

    private Long productId;

    private String key;

    private String value;

    private Instant createdAt;

    private Instant updatedAt;

}
