package com.bap.ecommerce.api.module.cart.operation;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Operation(
        description = "Return all products currently in the user's shopping cart",
        summary = "Retrieve all cart items by user id",
        parameters = @Parameter(name = "userId", description = "User ID", example = "2"),
        responses = {
                @ApiResponse(responseCode = "200", description = "Get all user's cart item successfully!",
                        content = @Content(mediaType = "application/json",
                                examples = {
                                        @ExampleObject(
                                                value = "@get_all_cart_item_by_user_id_200.json")
                                }
                        )
                ),
                @ApiResponse(responseCode = "403", description = "Unauthorized",
                        content = @Content(mediaType = "application/json",
                                examples = {
                                        @ExampleObject(
                                                value = "@get_all_cart_item_by_user_id_403.json"
                                        )
                                }
                        )
                ),
                @ApiResponse(responseCode = "404", description = "Resource not found!",
                        content = @Content(mediaType = "application/json",
                                examples = {
                                        @ExampleObject(
                                                value = "@get_all_cart_item_by_user_id_404.json"
                                        )
                                }
                        )
                )
        }
)
public @interface GetAllItemsByUserIdOperation {
}
