package com.bap.ecommerce.api.module.order.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bap.ecommerce.api.common.entity.Order;
import com.bap.ecommerce.api.common.vo.OrderStatusE;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query("SELECT od FROM Order od WHERE od.user.id = :id")
    List<Order> getAllOrderByUserId(Long id);

    @Query("SELECT od FROM Order od WHERE od.status = :status")
    List<Order> getAllOrderByStatus(OrderStatusE status);
}
