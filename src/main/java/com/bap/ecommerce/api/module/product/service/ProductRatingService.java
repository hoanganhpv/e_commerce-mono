package com.bap.ecommerce.api.module.product.service;

import com.bap.ecommerce.api.module.product.dto.PageRatingResDto;
import com.bap.ecommerce.api.module.product.dto.ProductRatingReqDto;
import org.springframework.stereotype.Service;

@Service
public interface ProductRatingService {

    PageRatingResDto findAllProductRatingByProductId(Long productId, int perPage, int currentPage);

    void saveProductRating(Long userId, ProductRatingReqDto productRating);

}
