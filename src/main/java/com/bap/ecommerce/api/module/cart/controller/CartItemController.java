package com.bap.ecommerce.api.module.cart.controller;

import com.bap.ecommerce.api.common.dto.ResponseDto;
import com.bap.ecommerce.api.module.cart.dto.CartItemQuantityRequestDto;
import com.bap.ecommerce.api.module.cart.dto.CartItemReqDto;
import com.bap.ecommerce.api.module.cart.operation.CreateNewCartItemOperation;
import com.bap.ecommerce.api.module.cart.operation.DeleteCartItemOperation;
import com.bap.ecommerce.api.module.cart.operation.GetAllItemsByUserIdOperation;
import com.bap.ecommerce.api.module.cart.operation.UpdateCartItemListOperation;
import com.bap.ecommerce.api.module.cart.service.CartItemService;
import com.bap.ecommerce.api.module.user.dto.ResponseDataDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/v1/cart-items")
@Tag(name = "carts")
public class CartItemController {

    @Autowired
    private CartItemService cartItemService;

    @CreateNewCartItemOperation
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> createNewCartItem(@Validated @RequestBody CartItemReqDto cartItemReqDto) {
        cartItemService.createNewCartItem(cartItemReqDto);
        ResponseDto response = new ResponseDto();
        response.setCode(HttpStatus.OK.value());
        response.setStatus(HttpStatus.OK.name());
        response.setMessage("Create new cart item successfully!");
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetAllItemsByUserIdOperation
    @GetMapping(value = "/{userId}")
    public ResponseEntity<ResponseDataDto> getAllItemsByUserId(@PathVariable("userId") Long userId) {
        ResponseDataDto responseData = new ResponseDataDto();
        responseData.setStatus(HttpStatus.OK.name());
        responseData.setCode(HttpStatus.OK.value());
        responseData.setMessage("Get all user's cart item successfully!");
        responseData.setData(cartItemService.getAllCartItemsByUserId(userId));
        return ResponseEntity.status(HttpStatus.OK).body(responseData);
    }

    @UpdateCartItemListOperation
    @PutMapping(value = "/update-all/{userId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> updateCartItemList(
            @PathVariable("userId") Long userId,
            @Validated @RequestBody CartItemQuantityRequestDto cartItemQuantityRequestDtoList) {
        cartItemService.updateCartItems(userId, cartItemQuantityRequestDtoList);
        ResponseDto response = new ResponseDto();
        response.setStatus(HttpStatus.OK.name());
        response.setCode(HttpStatus.OK.value());
        response.setMessage("Update all user's cart item successfully!");
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @DeleteCartItemOperation
    @DeleteMapping(value = "/{userId}/{cartItemId}")
    public ResponseEntity<ResponseDto> deleteCartItem(@PathVariable("userId") Long userId,
                                                      @PathVariable("cartItemId") Long cartItemId) {
        cartItemService.deleteCartItem(userId, cartItemId);
        ResponseDto response = new ResponseDto();
        response.setStatus(HttpStatus.OK.name());
        response.setCode(HttpStatus.OK.value());
        response.setMessage("Delete cart item successfully!");
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
