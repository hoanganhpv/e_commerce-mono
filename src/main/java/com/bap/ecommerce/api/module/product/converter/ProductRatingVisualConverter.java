package com.bap.ecommerce.api.module.product.converter;

import org.springframework.stereotype.Component;

import com.bap.ecommerce.api.common.entity.ProductRatingVisual;
import com.bap.ecommerce.api.module.product.dto.ProductRatingVisualDto;

@Component
public class ProductRatingVisualConverter {

    /**
     * Convert Product entity to ProductRatingVisualDto
     *
     * @param entity ProductRatingVisualEntity
     * @return ProductRatingVisualDto
     */
    public ProductRatingVisualDto toDto(ProductRatingVisual entity) {
        ProductRatingVisualDto dto = new ProductRatingVisualDto();
        dto.setId(entity.getId());
        dto.setType(entity.getType());
        dto.setUrl(entity.getUrl());
        dto.setProductRatingId(entity.getRating().getId());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setUpdatedAt(entity.getUpdatedAt());
        return dto;
    }
}
