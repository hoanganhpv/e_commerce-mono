package com.bap.ecommerce.api.module.product.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bap.ecommerce.api.common.entity.ProductOptionDetail;
import com.bap.ecommerce.api.common.vo.ProductStatusType;
import com.bap.ecommerce.api.module.product.converter.ProductOptionDetailConverter;
import com.bap.ecommerce.api.module.product.dto.ProductOptionDetailDto;
import com.bap.ecommerce.api.module.product.repository.ProductOptionDetailRepository;
import com.bap.ecommerce.api.module.product.service.ProductOptionDetailService;

@Service
public class ProductOptionDetailServiceImpl implements ProductOptionDetailService {

    @Autowired
    private ProductOptionDetailConverter productOptionDetailConverter;

    @Autowired
    private ProductOptionDetailRepository productOptionDetailRepository;

    /**
     * *
     * Find exactly Product Option Detail By productId and productOptionDetailId
     *
     * @param productId             This is id of product.
     * @param productOptionDetailId This is id of product option detail.
     * @return Product Option Detail with a List of Image and Video in each Product
     *         Option Detail
     */
    public ProductOptionDetailDto findProductOptionDetailByIdAndByProductId(Long productId,
            Long productOptionDetailId) {
        List<ProductStatusType> status = List.of(ProductStatusType.RECYCLE,
                ProductStatusType.DRAFT,
                ProductStatusType.DELETED);
        ProductOptionDetail productOptionDetail = this.productOptionDetailRepository
                .findProductOptionDetailByProductId(productId, productOptionDetailId, status);

        if (productOptionDetail == null) {
            return null;
        }

        return this.productOptionDetailConverter.toDto(productOptionDetail);
    }
}
