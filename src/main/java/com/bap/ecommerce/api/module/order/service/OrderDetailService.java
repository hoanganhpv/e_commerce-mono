package com.bap.ecommerce.api.module.order.service;

import com.bap.ecommerce.api.module.order.dto.OrderDetailDto;

import java.util.List;

public interface OrderDetailService {

    List<OrderDetailDto> getOrderDetailByOrderId(Long Id);
}
