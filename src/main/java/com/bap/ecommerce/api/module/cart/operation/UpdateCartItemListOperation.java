package com.bap.ecommerce.api.module.cart.operation;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Operation(
        description = "@update_cart_item_list_descriptions.json",
        summary = "Add a product to the user's shopping cart",
        requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                content = @Content(mediaType = "application/json",
                        examples = {
                                @ExampleObject(
                                        value = "@update_cart_item_list_request_body.json"
                                )
                        }
                )),
        responses = {
                @ApiResponse(responseCode = "200", description = "Create new cart item successfully",
                        content = @Content(mediaType = "application/json",
                                examples = {
                                        @ExampleObject(
                                                value = "@update_cart_item_list_200.json"
                                        )
                                }
                        )
                ),
                @ApiResponse(responseCode = "403", description = "Unauthorized",
                        content = @Content(mediaType = "application/json",
                                examples = {
                                        @ExampleObject(
                                                value = "@update_cart_item_list_403.json"
                                        )
                                }
                        )
                ),
                @ApiResponse(responseCode = "404", description = "Resource not found!",
                        content = @Content(mediaType = "application/json",
                                examples = {
                                        @ExampleObject(
                                                value = "@update_cart_item_list_404.json"
                                        )
                                }
                        )
                ),
                @ApiResponse(responseCode = "500", description = "Exceeded inventory quantity!",
                        content = @Content(mediaType = "application/json",
                                examples = {
                                        @ExampleObject(
                                                value = "@update_cart_item_list_500.json"
                                        )
                                }
                        )
                )
        }
)
public @interface UpdateCartItemListOperation {
}
