package com.bap.ecommerce.api.module.product.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bap.ecommerce.api.common.entity.ProductDescription;
import com.bap.ecommerce.api.common.vo.ProductStatusType;

@Repository
public interface ProductDescriptionRepository extends JpaRepository<ProductDescription, Long> {

    @Query("SELECT pd FROM ProductDescription pd JOIN pd.product p " +
            "WHERE p.id = :productId " +
            "AND p.status NOT IN (:status)")
    List<ProductDescription> findByProduct(@Param("productId") Long productId,
            @Param("status") List<ProductStatusType> status);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM product_description WHERE product_id = :id", nativeQuery = true)
    void deleteAllByProductId(@Param("id") Long id);
}
