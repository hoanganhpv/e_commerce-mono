package com.bap.ecommerce.api.module.product.service;

import com.bap.ecommerce.api.module.product.dto.ProductOptionDetailDto;

public interface ProductOptionDetailService {
    public ProductOptionDetailDto findProductOptionDetailByIdAndByProductId(Long productId, Long productOptionDetailId);
}
