package com.bap.ecommerce.api.module.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bap.ecommerce.api.common.entity.User;
import com.bap.ecommerce.api.common.entity.UserRefreshToken;

@Repository
public interface UserRefreshTokenRepository extends JpaRepository<UserRefreshToken, Long> {

    int deleteByUser(User user);

    Optional<UserRefreshToken> findUserRefreshTokenByToken(String token);
}
