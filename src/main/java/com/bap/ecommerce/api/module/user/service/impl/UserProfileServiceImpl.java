package com.bap.ecommerce.api.module.user.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import com.bap.ecommerce.api.common.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.bap.ecommerce.api.common.entity.UserProfile;
import com.bap.ecommerce.api.common.exception.InternalServerErrorException;
import com.bap.ecommerce.api.common.exception.ResourceNotFoundException;
import com.bap.ecommerce.api.module.user.dto.UpdatingUserProfileDto;
import com.bap.ecommerce.api.module.user.dto.UserProfileDto;
import com.bap.ecommerce.api.module.user.repository.UserProfileRepository;
import com.bap.ecommerce.api.module.user.repository.UserRepository;
import com.bap.ecommerce.api.module.user.service.UserProfileService;

@Service
public class UserProfileServiceImpl implements UserProfileService {

    private static final String IMAGE_PROFILE_PATH = "/images/profile";

    @Autowired
    UserProfileRepository userProfileRepository;

    @Autowired
    UserRepository usersRepository;

    @Value("${image.dir.profile}")
    private String dirLocation;

    @Value("${bap.ecommerce.domain}")
    private String domain;

    public static String sha256(final String base) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(base.getBytes("UTF-8"));
            final StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < hash.length; i++) {
                final String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Get all users profile.
     *
     * @return A list of users profile.
     */
    @Override
    public List<UserProfileDto> getAllUsersProfile() {
        return this.userProfileRepository.getAllUsersProfile();
    }

    /**
     * Get a user profile by ID.
     *
     * @param userId This is an ID of the user is also be an ID of the user profile.
     * @return The profile of the user.
     * @throws ResourceNotFoundException if the ID of the user profile not found.
     */
    @Override
    public UserProfileDto getUserProfileById(Long userId) {
        // Checking if the user exists or not
        if (this.usersRepository.findById(userId).isEmpty()) {
            throw new ResourceNotFoundException("User with id " + userId + " not found");
        }

        // Checking if the user profile exists or not
        if (this.userProfileRepository.findById(userId).isEmpty()) {
            throw new ResourceNotFoundException("User profile with id " + userId + " not found");
        }

        return this.userProfileRepository.getAllUserProfileById(userId);
    }

    /**
     * Get a user profile by name.
     *
     * @param fullName This is the name of those users profile.
     * @return The list users profile.
     */
    @Override
    public List<UserProfileDto> getUsersProfileByName(String fullName) {

        return this.userProfileRepository.getAllUsersByName(fullName);
    }

    /**
     * Update a user profile.
     *
     * @param userId                 This is an ID of the user is also be an ID of the
     *                           user profile.
     * @param userProfileRequest This is all information to update a user profile.
     * @throws ResourceNotFoundException if the ID of the user not found.
     */
    @Override
    public void updateUserProfile(Long userId, UpdatingUserProfileDto userProfileRequest) {

        // Checking if the user exists or not
        if (this.usersRepository.findById(userId).isEmpty()) {
            throw new ResourceNotFoundException("User with id " + userId + " not found");
        }

        // Checking if the user profile exists or not
        UserProfile userProfile = this.userProfileRepository
                .findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User profile with id " + userId + " not found"));

        userProfile.setFullName(userProfileRequest.getFullName());
        userProfile.setGender(userProfileRequest.getGender());
        userProfile.setBirthday(userProfileRequest.getBirthdayInDate());
        userProfile.setUpdatedAt(Instant.now());

        this.userProfileRepository.save(userProfile);
    }

    /**
     * Upload a user picture profile.
     *
     * @param userId  This is an ID of the user is also be an ID of the user profile.
     * @param file This is the picture from the user request.
     * @throws ResourceNotFoundException    if the ID of the user or the file not
     *                                      found.
     * @throws InternalServerErrorException if file upload fail.
     */
    @Override
    public void uploadProfilePicture(Long userId, MultipartFile file) {

        String decode = sha256(String.valueOf(System.currentTimeMillis()));

        // Checking if the user exists or not
        if (this.usersRepository.findById(userId).isEmpty()) {
            throw new ResourceNotFoundException("User with id " + userId + " not found");
        }

        // Checking if the user profile exists or not
        UserProfile userProfile = this.userProfileRepository
                .findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User profile with id " + userId + " not found"));

        if (file.isEmpty()) {
            throw new IllegalArgumentException("Please select a file to upload!");
        }

        try {
            // Generate a unique file name to avoid conflicts
            int lastDotIndex = file.getOriginalFilename().toString().lastIndexOf(".");
            String fileNameDecoded = decode + "." + file
                    .getOriginalFilename()
                    .toString()
                    .substring(lastDotIndex + 1);
            // Get the resource folder path
            Path resourcePath = Paths.get(dirLocation);

            // Formatting the path
            Path destinationFilePath = resourcePath
                    .resolve(Paths.get(fileNameDecoded))
                    .normalize();

            // Save the file to the specified path
            Files.copy(file.getInputStream(), destinationFilePath, StandardCopyOption.REPLACE_EXISTING);

            // Setting the path of picture profile as a relative path
            userProfile.setProfilePicture(domain + IMAGE_PROFILE_PATH + "/" + fileNameDecoded);
            this.userProfileRepository.save(userProfile);
        } catch (IOException e) {
            // Handle file upload failure
            throw new InternalServerErrorException("Failed to upload the file!");
        }
    }

    /**
     * Get a user picture profile by user ID.
     *
     * @param id This is an ID of the user is also be an ID of the user profile.
     * @return The picture of the user.
     * @throws ResourceNotFoundException if the ID of the user profile or the
     *                                   picture not found.
     */
    @Override
    public String getProfilePictureByUserId(Long id) {

        // Checking if the user exists or not
        if (this.usersRepository.findById(id).isEmpty()) {
            throw new ResourceNotFoundException("User with id " + id + " not found");
        }

        // Checking if the user profile exists or not
        UserProfile userProfile = this.userProfileRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User profile with id " + id + " not found"));

        if (userProfile.getProfilePicture() == null) {
            return null;
        }

        return userProfile.getProfilePicture();
    }
}
