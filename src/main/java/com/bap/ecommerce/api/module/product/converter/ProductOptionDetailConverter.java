package com.bap.ecommerce.api.module.product.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bap.ecommerce.api.common.entity.ProductOptionDetail;
import com.bap.ecommerce.api.module.product.dto.ProductOptionDetailDto;
import com.bap.ecommerce.api.module.product.dto.ProductVisualDto;

@Component
public class ProductOptionDetailConverter {

    @Autowired
    private ProductVisualConverter productVisualConverter;

    /**
     * Convert ProductOptionDetailEntity to ProductOptionDetailDto
     *
     * @param entity ProductOptionDetailEntity
     * @return ProductOptionDetailDto with List ProductVisual prefer to
     *         productOptionDetailId
     */
    public ProductOptionDetailDto toDto(ProductOptionDetail entity) {
        ProductOptionDetailDto dto = new ProductOptionDetailDto();
        dto.setId(entity.getId());
        dto.setProductOptionId(entity.getProductOption().getId());
        dto.setValue(entity.getValue());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setUpdatedAt(entity.getUpdatedAt());
        dto.setProductId(entity.getProduct().getId());
        List<ProductVisualDto> listProductOptionDetailVisuals = entity.getProductVisuals()
                .stream()
                .map(e -> productVisualConverter.toDto(e))
                .collect(Collectors.toList());
        dto.setListProductOptionDetailVisuals(listProductOptionDetailVisuals);
        return dto;
    }
}
