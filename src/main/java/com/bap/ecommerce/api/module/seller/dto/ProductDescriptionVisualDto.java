package com.bap.ecommerce.api.module.seller.dto;

import com.bap.ecommerce.api.common.vo.ProductDescriptionVisualType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDescriptionVisualDto {

    private String url;
    private ProductDescriptionVisualType type;
}
