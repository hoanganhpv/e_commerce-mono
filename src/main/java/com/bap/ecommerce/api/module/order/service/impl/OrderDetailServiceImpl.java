package com.bap.ecommerce.api.module.order.service.impl;

import com.bap.ecommerce.api.module.order.dto.OrderDetailDto;
import com.bap.ecommerce.api.module.order.repository.OrderDetailRepository;
import com.bap.ecommerce.api.module.order.service.OrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderDetailServiceImpl implements OrderDetailService {

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    public OrderDetailServiceImpl() {
    }

    public List<OrderDetailDto> getOrderDetailByOrderId(Long id) {
        return this.orderDetailRepository.getOrderDetailByOrderId(id);
    }
}
