package com.bap.ecommerce.api.module.product.converter;

import org.springframework.stereotype.Component;

import com.bap.ecommerce.api.common.entity.ProductVisual;
import com.bap.ecommerce.api.module.product.dto.ProductVisualDto;

@Component
public class ProductVisualConverter {

    public ProductVisualDto toDto(ProductVisual entity) {
        ProductVisualDto dto = new ProductVisualDto();
        dto.setId(entity.getId());
        dto.setSlug(entity.getSlug());
        dto.setType(entity.getType());
        dto.setUrl(entity.getUrl());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setUpdatedAt(entity.getUpdatedAt());
        return dto;
    }
}
