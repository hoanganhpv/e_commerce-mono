package com.bap.ecommerce.api.module.order.service;

import java.util.List;

import com.bap.ecommerce.api.common.entity.Order;
import com.bap.ecommerce.api.common.vo.OrderStatusE;
import com.bap.ecommerce.api.module.order.dto.CreateOrderRequestDto;
import com.bap.ecommerce.api.module.order.dto.OrderDto;

public interface OrderService {

    OrderDto getOrderById(Long id);

    List<OrderDto> getAllOrderByUserId(Long id);

    List<OrderDto> getOrderByStatus(OrderStatusE status);

    void updateStatusOrder(Long id, OrderStatusE orderStatus);

    void saveOrder(CreateOrderRequestDto createOrderRequestDto);

    public OrderDto mapperOrderEntityToDto(Order order);
}
