package com.bap.ecommerce.api.module.product.service;

import com.bap.ecommerce.api.module.product.dto.ProductDto;
import com.bap.ecommerce.api.module.product.dto.ProductItemsResDto;
import com.bap.ecommerce.api.module.product.dto.ProductListDto;

public interface ProductService {
    public ProductDto findProductByProductId(Long productId);

    ProductListDto findPageProduct(
            int perPage, int currentPage);

    ProductItemsResDto getProduct(Long id);
}
