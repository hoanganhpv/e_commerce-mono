package com.bap.ecommerce.api.module.product.dto;


import java.math.BigDecimal;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductItemDto {

    private Long id;

    private Long productId;

    private BigDecimal price;

    private Integer quantity;

    private List<ProductItemDetailDto> productItemDetails;
}
