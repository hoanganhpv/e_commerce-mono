package com.bap.ecommerce.api.module.user.dto;

import com.bap.ecommerce.api.common.dto.ResponseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDataDto extends ResponseDto {
    private Object data;
}
