package com.bap.ecommerce.api.module.product.service;

import com.bap.ecommerce.api.module.product.dto.ProductSearchByCategoryPageResDto;
import com.bap.ecommerce.api.module.product.dto.ProductSearchPageDto;

public interface PageService {

    ProductSearchPageDto pageableListResultSearch(String keyword, int pageNumber, int size);

    ProductSearchByCategoryPageResDto pageListProductWithCategory(short parent_id, int pageNumber, int size);
}
