package com.bap.ecommerce.api.module.product.dto;

import com.bap.ecommerce.api.common.vo.ProductStatusType;

import java.math.BigDecimal;

import java.util.List;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ProductDto {
    // Product
    private Long id;

    private String title;

    private String slug;

    private String sku;

    private Long CategoryId;

    private Integer quantityInStock;

    private BigDecimal price;

    private BigDecimal percentDiscount;

    private BigDecimal priceSales;

    private ProductStatusType status;

    private float star_rating;

    private List<ProductVisualDto> listMediaProduct;

}
